import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { MatToolbarModule, MatTabsModule, MatInputModule, MatButtonModule, MatTableModule, MatPaginator, MatPaginatorModule, MatSortModule } from '@angular/material';

import { AppComponent } from './app.component';
import { EncryptComponent } from './encrypt/encrypt.component';
import { DecryptComponent } from './decrypt/decrypt.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RestCallsService } from './rest-calls.service';
import { HttpClient } from 'selenium-webdriver/http';
import { HttpClientModule } from '@angular/common/http';
import { LogDatatableComponent } from './log-datatable/log-datatable.component';

@NgModule({
  declarations: [
    AppComponent,
    EncryptComponent,
    DecryptComponent,
    LogDatatableComponent
  ],
  imports: [
    BrowserModule,
    MatToolbarModule,
    MatTabsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    HttpClientModule,
    MatPaginatorModule,
    MatSortModule
  ],
  providers: [
    RestCallsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
