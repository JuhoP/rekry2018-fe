import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatToolbarModule, MatTabsModule, MatInputModule, MatButtonModule, MatTableModule, MatPaginator, MatPaginatorModule, MatSortModule } from '@angular/material';
 

import { DecryptComponent } from './decrypt.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RestCallsService } from '../rest-calls.service';

describe('DecryptComponent', () => {
  let component: DecryptComponent;
  let fixture: ComponentFixture<DecryptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecryptComponent ],
      imports: [
        BrowserModule,
        MatToolbarModule,
        MatTabsModule,
        BrowserAnimationsModule,
        MatInputModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule,
        MatTableModule,
        HttpClientModule,
        MatPaginatorModule,
        MatSortModule
      ],
      providers: [
        RestCallsService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecryptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
