import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RestCallsService } from '../rest-calls.service';
import { FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'decrypt',
  templateUrl: './decrypt.component.html',
  styleUrls: ['./decrypt.component.css']
})
export class DecryptComponent implements OnInit {

  @Output() logdata = new EventEmitter<any>();

  private cypherFC:  FormControl = new FormControl('', [Validators.required]);
  private seedFC:  FormControl = new FormControl('', [Validators.required]);
  private formArray: FormArray;
  private result;
  private error;

  constructor(
    private _rest: RestCallsService
  ) { 
    this.formArray = new FormArray([this.cypherFC, this.seedFC]);
  }

  ngOnInit() { }

  setDecrypt(){
    let body = {
      decryptAmount: this.seedFC.value,
      decryptString: this.cypherFC.value
    }
    this._rest.postDecrypt(body).subscribe( res => {
      this.logdata.emit(res);
      this.result = res.decryptResult;
      this.error = null;
    }, err => {
      this.result = null;
      this.error = err;
    });

  }

}
