import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatToolbarModule, MatTabsModule, MatInputModule, MatButtonModule, MatTableModule, MatPaginator, MatPaginatorModule, MatSortModule } from '@angular/material';
 
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { EncryptComponent } from './encrypt.component';
import { RestCallsService } from '../rest-calls.service';

describe('EncryptComponent', () => {
  let component: EncryptComponent;
  let fixture: ComponentFixture<EncryptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncryptComponent ],
      imports: [
        BrowserModule,
        MatToolbarModule,
        MatTabsModule,
        BrowserAnimationsModule,
        MatInputModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule,
        MatTableModule,
        HttpClientModule,
        MatPaginatorModule,
        MatSortModule
      ],
      providers: [RestCallsService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncryptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
