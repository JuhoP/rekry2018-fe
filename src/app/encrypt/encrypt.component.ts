import { Component, OnInit, ViewChild, AfterContentInit, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { RestCallsService } from '../rest-calls.service';

@Component({
  selector: 'encrypt',
  templateUrl: './encrypt.component.html',
  styleUrls: ['./encrypt.component.css']
})
export class EncryptComponent {

  @Output() logdata = new EventEmitter<any>();

  private formArray: FormArray;
  private plaintextFC: FormControl = new FormControl('', [Validators.required]);
  private seedFC: FormControl = new FormControl('', [Validators.required]);
  private result;
  private error;

  constructor(
    private _rest: RestCallsService
  ) {
    this.formArray = new FormArray([this.plaintextFC, this.seedFC]);
  }

  setEncrypt() {
    let body = {
      encryptString: this.plaintextFC.value,
      encryptAmount: this.seedFC.value
    }
    this._rest.postEncrypt(body).subscribe(res => {
      this.logdata.emit(res);
      this.result = res.encryptResult;
      this.error = null;
    }, err => {
      this.error = err;
      this.result = null;
      console.log(err);
    });
  }

}
