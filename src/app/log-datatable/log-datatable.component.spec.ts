import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatToolbarModule, MatTabsModule, MatInputModule, MatButtonModule, MatTableModule, MatPaginator, MatPaginatorModule, MatSortModule } from '@angular/material';
 
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RestCallsService } from '../rest-calls.service';
import { LogDatatableComponent } from './log-datatable.component';

describe('LogDatatableComponent', () => {
  let component: LogDatatableComponent;
  let fixture: ComponentFixture<LogDatatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogDatatableComponent ],
      imports: [
        BrowserModule,
        MatToolbarModule,
        MatTabsModule,
        BrowserAnimationsModule,
        MatInputModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule,
        MatTableModule,
        HttpClientModule,
        MatPaginatorModule,
        MatSortModule
      ],
      providers: [
        RestCallsService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogDatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
