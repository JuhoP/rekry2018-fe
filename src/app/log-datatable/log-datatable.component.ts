import { Component, OnInit, ViewChild, AfterContentInit, AfterViewInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { RestCallsService } from '../rest-calls.service';


@Component({
  selector: 'log-datatable',
  templateUrl: './log-datatable.component.html',
  styleUrls: ['./log-datatable.component.css']
})
export class LogDatatableComponent implements AfterViewInit, OnChanges {

  @Input() data;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ['ID', 'TYPE', 'REQUEST', 'RESPONSE', 'SEED'];
  dataSource = new MatTableDataSource<Element>();

  constructor(
  ) {}

  ngAfterViewInit(){
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnChanges(ch: SimpleChanges){
    if(ch.data.currentValue){
      this.dataSource.data = this.data.log;
    }
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

}
//{logId: 998, logType: "encrypt", logBefore: "!", logAfter: "3"", logSeed: "1"}
export interface Element {
  logId: any,
  logType: any,
  logBefore: any,
  logAfter: any,
  logSeed: any
}
