import { TestBed, inject } from '@angular/core/testing';

import { RestCallsService } from './rest-calls.service';
import { HttpClientModule } from '@angular/common/http';

describe('RestCallsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestCallsService],
      imports: [HttpClientModule]
    });
  });

  it('should be created', inject([RestCallsService], (service: RestCallsService) => {
    expect(service).toBeTruthy();
  }));
});
