import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RestCallsService {

  private base: string = 'http://localhost:3000';
  //private base:  string = 'http://ebmeds.org/rekry2018/api'
  private urlEnctrypt: string = this.base + '/encrypt';
  private urlDecrypt: string = this.base + '/decrypt'

  constructor(
    private http: HttpClient
  ) { }

  postEncrypt(body): Observable<any> {
    return this.http.post(this.urlEnctrypt, body);
  }
  postDecrypt(body): Observable<any>{
    return this.http.post(this.urlDecrypt, body);
  }


}
